var products = [
  {
    id: 1,
    title: "a title",
    img: "https://picsum.photos/200/300",
    price: 3000,
  },
  {
    id: 2,
    title: "another title",
    img: "https://picsum.photos/200/300",
    price: 66000,
  },
  {
    id: 3,
    title: "title8",
    img: "https://picsum.photos/200/300",
    price: 57000,
  },
  {
    id: 4,
    title: "title 4",
    img: "https://picsum.photos/200/300",
    price: 8000,
  },
  {
    id: 5,
    title: "title 5",
    img: "https://picsum.photos/200/300",
    price: 700,
  },
];
export default products;
